const Order = require("../models/Order");
const Product = require("../models/Product");
const auth = require("../auth");
const User = require("../models/User");


//Check out order
module.exports.checkOut = (data) =>{

    return User.findById(data.userId).then((result) => {

    if (result.isAdmin === true){

        return "Unable to make any transactions because you are an Admin";

    } else {

    return Product.findById(data.productId).then((result, err) => {

            let newOrder = new Order({
                userId: data.userId,
                totalAmount: data.quantity * result.price,
                products: [{
                    productId: data.productId,
                    quantity: data.quantity,
            }]
     })

    return newOrder.save().then((order,error)=>{
            if(error){
                    return false;
            } else {

            	let product = {
            		orders: [{
            			orderId: data.userId,
            			quantity: data.quantity
            		}]
            	}

            	return Product.findByIdAndUpdate(data.productId, {$push: product}).then((order,error)=>{
    

                            if(error){
                                return false;
                            } else {
                                return "Order has been successfully placed"; 

            		}
            	})

                
            }
        })

})
}
})
}


module.exports.userOrders = (user) =>{

    return User.findById(user.userId).then((result) => {

        if (result.isAdmin === true){


            return "You are not allowed to view transactions";

        } else {

            return Order.find({userId:user.userId}).then(result=>{return result;});
        }
    })
}


