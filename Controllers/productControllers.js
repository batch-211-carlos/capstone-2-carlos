const Product = require("../models/Product");
const User = require("../models/User");
const Order = require("../models/Order");
const auth = require("../auth");


//Create Product
module.exports.newProduct = (data) =>{

    if (data.isAdmin){
        
        let newProduct = new Product({
            name : data.product.name,
            description : data.product.description,
            price : data.product.price
            })
    return newProduct.save().then((product,error)=>{
        if(error){
            return false;
        } else {
            return newProduct;
        }
    })
    } else {
        return Promise.resolve("Unauthorized to add new product.");
 }
}

// Retrieve all products
module.exports.getAllProducts = () =>{
    return Product.find({}).then(result=>{
        return result;
    });
};

//Get active products only
module.exports.getActiveProducts = () =>{
    return Product.find({isActive:true}).then(result=>{
        return result;
    });
};

//Retrieve single product
module.exports.getSingleProduct= (reqParams)=>{
    return Product.findById(reqParams.productId).then(result=>{
        return result;
    });
};

//Update product info
module.exports.updateProductInfo = (data)=>{

   if (data.isAdmin){

        let updateProduct = {
            name: data.product.name,
            description: data.product.description,
            price: data.product.price
        };

        return Product.findByIdAndUpdate(data.productId,updateProduct).then((product,error)=>{
            if(error){
                return false;
            } else {
                return Product.findById(data.productId).then(result=>{return result;});
            };
        }); 

    }  else {
        return Promise.resolve("You are not allowed to update a product.");
    }
};

//Archiving a product
module.exports.archiveProduct = (data)=>{

    if (data.isAdmin){

        let archivedProduct = {

            isActive: false
            
        };

        return Product.findByIdAndUpdate(data.productId,archivedProduct).then((product,error)=>{
            if(error){
                return false;
            } else {
                return Product.findById(data.productId).then(result=>{return result;});
            };
        }); 

    }  else {
        return Promise.resolve("You are not allowed to archived a product.");
    }
};


//Activate Product
module.exports.activateProduct = (data)=>{

    if (data.isAdmin){

        let archivedProduct = {isActive: true};

        return Product.findByIdAndUpdate(data.productId,archivedProduct).then((product,error)=>{
            if(error){
                return false;
            } else {
                return Product.findById(data.productId).then(result=>{return result;});
            };
        }); 

    }  else {
        return Promise.resolve("You are not allowed to archived a product.");
    }
};










