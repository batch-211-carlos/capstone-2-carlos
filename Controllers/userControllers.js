const User = require("../models/User");
const bcrypt = require("bcrypt");
const auth = require("../auth");

//User Registration



module.exports.registerUser = (reqBody) =>{
    let newUser = new User({
        firstName : reqBody.firstName,
        lastName : reqBody.lastName,
        email : reqBody.email,
        mobileNo : reqBody.mobileNo,
        password : bcrypt.hashSync(reqBody.password,10)
    })

    return User.find({email:reqBody.email}).then(result => {
                if(result.length > 0) {
                    return {message: "Email Already Used!"}
                }
                else {
                    return newUser.save().then((user, error) => {
                if (error) {
                    return false;
                }
                else {
                    return { 
                        message: `Successfully registered!`,
                        data: user
                    };
                }
            });
        }
    });
    
};





//Authenticate User

module.exports.authUser = (reqBody) =>{
    return User.findOne({email:reqBody.email}).then(result=>{
        if(result==null){
            return false;
        }else{
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password,result.password);
            if(isPasswordCorrect){
                return { access: auth.createAccessToken(result)}
            }else{

                return false
            };
        };
    });
};

//Retrieve all users
module.exports.getAllUser = () =>{
    return User.find({}).then(result=>{
        return result;
    });
};

//Retrieve User
module.exports.getUser = (data) =>{
    return User.findById(data.userId).then(result=>{
        result.password = "";
        return result;
    });
};


//Set User to Admin - Stretch Goal
module.exports.setUserasAdmin = (reqParams) =>{
    let updatedAdmin ={
        isAdmin: true
    };
    return User.findByIdAndUpdate(reqParams.userId,updatedAdmin).then((result=>{
        return result;
    }));
};









