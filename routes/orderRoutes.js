const express = require('express');
const router = express.Router();
const orderController = require('../controllers/orderControllers');
const auth = require('../auth');


//order route
router.post("/order", auth.verify, (req,res)=>{

	const data = {
		userId: auth.decode(req.headers.authorization).id,
        totalAmount: req.body.totalAmount,
        productId: req.body.productId,
        quantity: req.body.quantity
	}

	console.log(data);
	
	orderController.checkOut(data).then(resultFromController=>res.send(resultFromController));
});



router.get("/userOrders", auth.verify, (req,res)=>{

	const user = {
		userId: auth.decode(req.headers.authorization).id,
	}
	
	console.log(user);

	orderController.userOrders(user).then(resultFromController=>res.send(resultFromController));
});





module.exports = router;

