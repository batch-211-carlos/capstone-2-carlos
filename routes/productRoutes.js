const express = require("express");
const router = express.Router();
const productController = require("../controllers/productControllers");
const auth = require("../auth");

//Create Product Route
router.post("/addProduct", auth.verify, (req,res)=>{

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}
	
	productController.newProduct(data).then(resultFromController=>res.send(resultFromController));
});


// Retrieve all products Route
router.get("/allProducts",(req,res)=>{
	productController.getAllProducts().then(resultFromController=>res.send(resultFromController));
});

//Active Products route
router.get("/allActiveProducts",(req,res)=>{
	productController.getActiveProducts().then(resultFromController=>res.send(resultFromController));
});

//retrieve a single product
router.get("/singleproduct/:productId",(req,res)=>{
	productController.getSingleProduct(req.params).then(resultFromController=>res.send(resultFromController));
});

//update product info route
router.put("/updateProdInfo/:productId",auth.verify,(req,res)=>{

	const data = {
		productId: req.params.productId,
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data);

	productController.updateProductInfo(data).then(resultFromController=>res.send(resultFromController));
});

//Archive product route (admin)
router.put("/archivedProduct/:productId",auth.verify,(req,res)=>{

	const data = {
		productId: req.params.productId,
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data);

	productController.archiveProduct(data).then(resultFromController=>res.send(resultFromController));
});


module.exports = router;


//Activate a product route (admin)
router.put("/activateProduct/:productId",auth.verify,(req,res)=>{

	const data = {
		productId: req.params.productId,
		isActive: req.body.isActive,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	console.log(data);

	productController.activateProduct(data).then(resultFromController=>res.send(resultFromController));
});





