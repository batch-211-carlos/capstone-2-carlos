const express = require("express");
const router = express.Router();
const userController = require("../controllers/userControllers");
const auth = require("../auth");


//Registration Route
router.post("/register",(req,res)=>{
	userController.registerUser(req.body).then(resultFromController=>res.send(resultFromController));
})

//Auth Route

router.post("/login",(req,res)=>{
	userController.authUser(req.body).then(resultFromController=>res.send(resultFromController));
});

//Retrieve all users
router.get("/allUsers",(req,res)=>{

    userController.getAllUser(req.body).then(resultFromController=>res.send(resultFromController));
})

//Retrieve user Admin
router.post("/retrieveUser", auth.verify, (req,res)=>{


	const userData = auth.decode(req.headers.authorization);

    userController.getUser({userId:userData.id}).then(resultFromController=>res.send(resultFromController));
})



//Set Admin - Stretch Goal
router.put("/setAdmin/:userId",auth.verify,(req,res)=>{
	userController.setUserasAdmin(req.params,req.body).then(resultFromController=>res.send(resultFromController));
});



module.exports = router;


